# require "application_system_test_case"
#
# class TriageQueuesTest < ApplicationSystemTestCase
#   setup do
#     @triage_queue = triage_queues(:one)
#   end
#
#   test "visiting the index" do
#     visit triage_queues_url
#     assert_selector "h1", text: "Triage Queues"
#   end
#
#   test "creating a Triage queue" do
#     visit triage_queues_url
#     click_on "New Triage Queue"
#
#     fill_in "Notes", with: @triage_queue.notes
#     fill_in "Order", with: @triage_queue.order
#     fill_in "Patient", with: @triage_queue.patient
#     click_on "Create Triage queue"
#
#     assert_text "Triage queue was successfully created"
#     click_on "Back"
#   end
#
#   test "updating a Triage queue" do
#     visit triage_queues_url
#     click_on "Edit", match: :first
#
#     fill_in "Notes", with: @triage_queue.notes
#     fill_in "Order", with: @triage_queue.order
#     fill_in "Patientid", with: @triage_queue.patientID
#     click_on "Update Triage queue"
#
#     assert_text "Triage queue was successfully updated"
#     click_on "Back"
#   end
#
#   test "destroying a Triage queue" do
#     visit triage_queues_url
#     page.accept_confirm do
#       click_on "Destroy", match: :first
#     end
#
#     assert_text "Triage queue was successfully destroyed"
#   end
# end
