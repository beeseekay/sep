# require "application_system_test_case"
#
# class BookingsTest < ApplicationSystemTestCase
#   setup do
#     @booking = bookings(:one)
#   end
#
#   test "visiting the index" do
#     visit bookings_url
#     assert_selector "h1", text: "Bookings"
#   end
#
#   test "creating a Booking" do
#     visit bookings_url
#     click_on "New Booking"
#
#     fill_in "Datebooked", with: @booking.dateBooked
#     fill_in "Enddt,", with: @booking.endDt,
#     fill_in "Notes,", with: @booking.notes,
#     fill_in "Patientid,", with: @booking.patientID,
#     fill_in "Practitionersid,", with: @booking.practitionersID,
#     fill_in "Roomid,", with: @booking.roomID,
#     fill_in "Startdt,", with: @booking.startDt,
#     fill_in "Status,", with: @booking.status,
#     click_on "Create Booking"
#
#     assert_text "Booking was successfully created"
#     click_on "Back"
#   end
#
#   test "updating a Booking" do
#     visit bookings_url
#     click_on "Edit", match: :first
#
#     fill_in "Datebooked", with: @booking.dateBooked
#     fill_in "Enddt,", with: @booking.endDt,
#     fill_in "Notes,", with: @booking.notes,
#     fill_in "Patientid,", with: @booking.patientID,
#     fill_in "Practitionersid,", with: @booking.practitionersID,
#     fill_in "Roomid,", with: @booking.roomID,
#     fill_in "Startdt,", with: @booking.startDt,
#     fill_in "Status,", with: @booking.status,
#     click_on "Update Booking"
#
#     assert_text "Booking was successfully updated"
#     click_on "Back"
#   end
#
#   test "destroying a Booking" do
#     visit bookings_url
#     page.accept_confirm do
#       click_on "Destroy", match: :first
#     end
#
#     assert_text "Booking was successfully destroyed"
#   end
# end
