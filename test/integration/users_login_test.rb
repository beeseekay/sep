# require 'test_helper'
#
# class UsersLoginTest < ActionDispatch::IntegrationTest
#   test 'login with invalid information' do
#     get login_path
#     assert_template '/login'
#     post login_path, params: { session: { staffID: '1', password: 'invalid' } }
#     assert_template '/login'
#     assert_not flash.empty?
#     get root_path
#     assert_template '/login'
#   end
#
#   test 'login with valid information' do
#     get login_path
#     assert_template 'sessions/new'
#     post login_path, params: { session: { staffID: '1', password: 'password' } }
#     assert_template '/'
#     assert flash.empty?
#   end
# end
