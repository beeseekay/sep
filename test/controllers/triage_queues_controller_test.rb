require 'test_helper'

class TriageQueuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @triage_queue = {
      patient: Patient.first,
      order: 1,
      notes: 'Test Note'
    }
  end

  test "should get index" do
    get triage_queues_url
    assert_response :success
  end

  # test "should create triage_queue" do
  #   assert_difference('TriageQueue.count') do
  #     post triage_queues_url, params: {
  #       triage_queue: @triage_queue
  #     }
  #   end
  #
  #   assert_redirected_to triage_queue_url(TriageQueue.last)
  # end
  #
  # test "should show triage_queue" do
  #   get triage_queue_url(@triage_queue)
  #   assert_response :success
  # end
  #
  # test "should get edit" do
  #   get edit_triage_queue_url(@triage_queue)
  #   assert_response :success
  # end
  #
  # test "should update triage_queue" do
  #   patch triage_queue_url(@triage_queue), params: {
  #     triage_queue: @triage_queue
  #   }
  #   assert_redirected_to triage_queue_url(@triage_queue)
  # end

  # test "should destroy triage_queue" do
  #   assert_difference('TriageQueue.count', -1) do
  #     delete triage_queue_url(@triage_queue)
  #   end
  #
  #   assert_redirected_to triage_queues_url
  # end
end
