# require 'test_helper'
#
class BookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @booking_params = {
      patient: Patient.first.id,
      room: Room.first.id,
      practitioners: [Staff.first.id],
      notes: "Test Notes",
      status: "Test Status"
    }
  end

  test "should get index" do
    get bookings_url
    assert_response :success
  end

  test "should get new" do
    get new_booking_url
    assert_response :success
  end

#   test "should create booking" do
#     assert_difference('Booking.count') do
#       post bookings_url, params: {
#         booking: {
#           patient: @booking.patient,
#           room: @booking.room,
#           practitioners: @booking.practitioners,
#           startDt: @booking.startDt,
#           endDt: @booking.endDt,
#           notes: @booking.notes,
#           status: @booking.status
#         }
#       }
#     end
#     assert_redirected_to booking_url(Booking.last)
#   end
#
#   test "should show booking" do
#     get booking_url(@booking)
#     assert_response :success
#   end
#
#   test "should get edit" do
#     get edit_booking_url(@booking)
#     assert_response :success
#   end
#
#   test "should update booking" do
#     patch bookings_url, params: {
#       booking: {
#         patient: @booking.patient,
#         room: @booking.room,
#         practitioners: @booking.practitioners,
#         startDt: @booking.startDt,
#         endDt: @booking.endDt,
#         notes: @booking.notes,
#         status: @booking.status
#       }
#     }
#     assert_redirected_to booking_url(@booking)
#   end
#
#   test "should destroy booking" do
#     assert_difference('Booking.count', -1) do
#       delete booking_url(@booking)
#     end
#     assert_redirected_to bookings_url
#   end
end
