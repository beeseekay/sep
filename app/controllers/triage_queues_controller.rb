class TriageQueuesController < ApplicationController
  before_action :set_triage_queue, only: [:show, :destroy]

  # GET /triage_queues
  def index
    @triage_queue = TriageQueue.all.order_by(order: :asc)
    @queue_item = TriageQueue.new
  end

  # POST /triage_queues
  def create
    @queue_item = TriageQueue.new(triage_queue_params)
    @queue_item.order = TriageQueue.count + 1 # Add to end of queue

    respond_to do |format|
      if TriageQueue.where(patient_id: @queue_item.patient.id).exists?
        format.html { redirect_to triage_queues_url, notice: 'Patient already exists in queue.' }
      elsif @queue_item.save
        format.html { redirect_to triage_queues_url, notice: 'Triage queue was successfully created.' }
      else
        format.html { redirect_to triage_queues_url }
        format.json { render json: @queue_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def reorder
    params[:order].each_with_index do |id, count|
      puts id, count
      TriageQueue.find(id).update_attributes(order: count)
    end

    respond_to do |format|
      format.html { redirect_to triage_queues_url, notice: 'Queue Saved.' }
      format.json { head :no_content }
    end
  end

  # DELETE /triage_queues/1
  def destroy
    @queue_item.destroy
    respond_to do |format|
      format.html { redirect_to triage_queues_url, notice: 'Patient was removed from queue.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_triage_queue
      @queue_item = TriageQueue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def triage_queue_params
      params.require(:triage_queue).permit(:patient, :order, :notes)
    end
end
