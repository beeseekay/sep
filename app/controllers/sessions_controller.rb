class SessionsController < ApplicationController
  skip_before_action :authorize

  def new
    if session[:user_id]
      redirect_to '/'
    end
  end

  def create
    user = Staff.find_by(refNb: params[:session][:staffID].to_i)
    if user and user.authenticate(params[:session][:password])
      # If the user exists AND the password entered is correct.
      # Save the user id inside the browser cookie. This is how we keep the user
      # logged in when they navigate around our website.
      session[:user_id] = user.id
      redirect_to '/'
    else
      flash[:failedLogin] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/login'
  end
end
