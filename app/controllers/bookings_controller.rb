class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  # GET /bookings
  def index
    if ['doctor', 'nurse'].include? @current_user.role
      @bookings = Booking.where(practitioner_ids: @current_user.id)
    else
      @bookings = Booking.all
    end
    @rooms = Room.all
    @patients = Patient.all
    @staff = Staff.all
  end

  # GET /bookings/1
  def show
  end

  # GET /bookings/new
  def new
    @booking = Booking.new
    @booking.startDt = DateTime.now
    @booking.endDt = DateTime.now
    if params[:patient]
      @booking.patient = params[:patient]
    end
  end

  # GET /bookings/1/edit
  def edit
  end

  # POST /bookings
  def create
    if booking_params[:practitioners] and booking_params[:practitioners].length > 0
      booking_params[:practitioners] = Staff.find(booking_params[:practitioners])
    end

    @booking = Booking.new(booking_params)

    respond_to do |format|
      if @booking.save
        queue_item = TriageQueue.find_by(patient_id: @booking.patient.id)
        if queue_item
          queue_item.destroy
        end
        format.html { redirect_to @booking, notice: 'Booking was successfully created.' }
        format.json { render :show, status: :created, location: @booking }
      else 
        format.html { redirect_to new_booking_path, notice: @booking.errors }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookings/1
  def update
    if booking_params[:practitioners] and booking_params[:practitioners].length > 0
      booking_params[:practitioners] = Staff.find(booking_params[:practitioners])
    end

    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to @booking, notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url, notice: 'Booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit!
    end
end
