class StaffsController < ApplicationController
  before_action :set_staff, only: [:show, :edit, :update, :destroy]

  # GET /staff
  def index
    @staffs = Staff.all
  end

  # GET /staff/1
  def show
    @upcoming_bookings = Booking.where(practitioner_ids:  @staff.id, :endDt.gt => DateTime.now).to_a
  end

  # GET /staff/new
  def new
    @staff = Staff.new
    @staff.build_name
  end

  # GET /staff/1/edit
  def edit
  end

  # POST /staff
  def create
    @staff = Staff.new(staff_params)
    @staff.refNb = Staff.count + 1

    respond_to do |format|
      if @staff.save
        format.html { redirect_to @staff, notice: 'Staff was successfully created.' }
        format.json { render :show, status: :created, location: @staff }
      else
        format.html { render :new }
        format.json { render json: @staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /staff/1
  def update
    respond_to do |format|
      if staff_params[:password].eql? ""
        staff_params.delete(:password)
        @staff.skip_password = true
      end
      puts staff_params

      if @staff.update(staff_params)
        format.html { redirect_to @staff, notice: 'Staff was successfully updated.' }
        format.json { render :show, status: :ok, location: @staff }
      else
        format.html { render :edit }
        format.json { render json: @staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /staff/1
  # Disabled for now, should disable accounts rather than deleting them
  # def destroy
  #   @staff.destroy
  #   respond_to do |format|
  #     format.html { redirect_to staffs_url, notice: 'Staff was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_staff
      @staff = Staff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def staff_params
      params.require(:staff).permit!
    end
end
