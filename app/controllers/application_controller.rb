class ApplicationController < ActionController::Base
  before_action :authorize
  protect_from_forgery with: :exception

  private
  def authorize
    @current_user = Staff.find(session[:user_id]) if session[:user_id]
    redirect_to '/login' unless @current_user
  end
end
