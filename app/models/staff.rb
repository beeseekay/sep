require_dependency 'generics'

class Staff
  include Mongoid::Document
  include ActiveModel::SecurePassword

  field :refNb
  field :password
  field :password_digest, :type => String
  attr_accessor :confirm_pass

  has_secure_password
  field :role

  # Personal Details
  embeds_one :name, class_name: "Name",  as: "nameObj"
  field :DOB, type: Date
  field :gender
  field :address
  embeds_many :phoneNumbers, class_name: "PhoneNumber", as: "phoneNumberObj"

  # Practitioner Details
  field :licenceNb
  field :licenceDate, type: Date
  field :department

  has_and_belongs_to_many :bookings, class_name: "Booking", inverse_of: "practitioners"

  accepts_nested_attributes_for :name
  validates_associated :name

  attr_accessor :skip_password
  validates :password, length: { minimum: 6, message: 'Password must be at least 6 characters.' }, unless: :skip_password
  validate :passCheck, unless: :skip_password

  validates :phoneNumbers, presence: { message: 'At least one phone number must be added.'}
  validates :gender, presence: { message: 'A gender must be specified.' }
  validates :DOB, presence: { minimum: 6, message: 'A date of birth is required.'}
  validates :address, presence: {message: 'Staff address must be entered.'}

  def fullName
    "#{self.name.first} #{self.name.last}"
  end

  def passCheck
    if self.password != self.confirm_pass
      errors.add(:confirm_pass, "Passwords do not match")
    end
  end

  # TODO: We need to validate :licenceNb, licenceDate and department IF :role is Dr.
end
