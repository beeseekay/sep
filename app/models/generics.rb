class Name
    include Mongoid::Document

    field :first
    field :last
    field :preferred
    field :title

  # TODO: How to validate this?
  validates :first, presence: { message: 'First name is required.' }
  validates :last, presence: { message: 'Last name is required.' }
  validates :title, presence: { message: 'Title is required.' }

  embedded_in :nameObj, polymorphic: true
end

class PhoneNumber
    include Mongoid::Document

    field :type
    field :number

    embedded_in :phoneNumberObj, polymorphic: true
end