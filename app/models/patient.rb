require_dependency 'generics'

class Patient
  include Mongoid::Document
  include Mongoid::Timestamps

  field :refNb, type: Integer
  embeds_one :personalDetails, class_name: "PersonalDetails"
  embeds_one :insuranceDetails, class_name: "InsuranceDetails"
  embeds_many :emergencyContacts, class_name: "EmergencyContact"
  #embeds_many :history, as: "Booking"
  #has_many :bookings, as: "Booking"
  has_one :triage_queue, class_name: "TriageQueue"

  accepts_nested_attributes_for :personalDetails
  validates_associated :personalDetails
  
  def fullName
    "#{self.personalDetails.name.first} #{self.personalDetails.name.last}"
  end
end

class PersonalDetails
  include Mongoid::Document

  embeds_one :name, class_name: "Name", as: "nameObj"
  field :gender
  field :DOB, type: Date
  field :email
  embeds_many :phoneNumbers, class_name: "PhoneNumber", as: "phoneNumberObj"
  field :address

  embedded_in :patient, class_name: "Patient", inverse_of: "personalDetails"

  validates_associated :name
  accepts_nested_attributes_for :name
  validates :gender, presence: { message: 'A gender must be specified.' }
  validates :DOB, presence: { message: 'A date of birth must be specified.'}

  validates :phoneNumbers, presence: {message: 'At least one phone number must be added.'}
end

class EmergencyContact
  include Mongoid::Document
  field :name
  field :relation
  field :address
  field :phoneNumber
  field :isNextOfKin, type: Boolean

  embedded_in :patient
end

class InsuranceDetails
  include Mongoid::Document

  field :medicareNumber
  field :medicareRef
  field :medicareExpiry, type: Date

  embedded_in :patient
end

