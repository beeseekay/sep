class TriageQueue
  include Mongoid::Document
  belongs_to :patient, class_name: "Patient"
  field :order, type: Integer
  field :notes, type: String
end
