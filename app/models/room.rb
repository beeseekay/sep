class Room
  include Mongoid::Document
  field :refNb, type: String 
  field :department, type: String

  validates :refNb, presence: {message: 'Room number must be specified.'}
  validates_format_of :refNb, :with => /\A([^@\s]+)-([^@\s]+)\Z/i, message: 'Room format must floor-room.'
  validates :department, presence: {message: 'Department must be specified.'}

end
