class Booking
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :room, class_name: "Room", validate: false
  belongs_to :patient, class_name: "Patient", validate: false
  has_and_belongs_to_many :practitioners, class_name: "Staff", validate: false
  field :startDt, type: DateTime
  field :endDt, type: DateTime
  field :status, type: String
  field :notes, type: String

  validates :startDt, presence: {message: 'A start date must be specified'}
  validates :endDt, presence: {message: 'An end date must be specified'}

end
