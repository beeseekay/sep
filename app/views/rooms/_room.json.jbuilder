json.extract! room, :id, :defNb, :department, :created_at, :updated_at
json.url room_url(room, format: :json)
