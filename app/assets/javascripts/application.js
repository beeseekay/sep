// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery3
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= javascript_include_tag "jquery.schedule.min"
//= javascript_include_tag "jquery.calendar.min"

// String formatter
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

const EmergencyContactHTML =
    "<tr>\n" +
    "  <td><input class='form-control' type='text' name='patient[emergencyContacts][{0}][name]'></td>\n" +
    "  <td><input class='form-control' type='text' name='patient[emergencyContacts][{0}][relation]'></td>\n" +
    "  <td><input class='form-control' type='text' name='patient[emergencyContacts][{0}][address]'></td>\n" +
    "  <td><input class='form-control' type='text' name='patient[emergencyContacts][{0}][phoneNumber]'></td>\n" +
    "  <td><input class='checkbox' type='checkbox' name='patient[emergencyContacts][{0}][isNextOfKin]'></td>\n" +
    "</tr>";

const PatientPhoneNumberHTML =
    "<tr>\n" +
    "  <td><select class='form-control' name='patient[personalDetails_attributes][phoneNumbers][{0}][type]'>\n" +
    "    <option value='mobile'>Mobile</option>\n" +
    "    <option value='home'>Home</option>\n" +
    "    <option value='work'>Work</option>\n" +
    "  </select></td>\n" +
    "  <td><input class='form-control' type='text' name='patient[personalDetails_attributes][phoneNumbers][{0}][number]'></td>\n" +
    "</tr>";

const StaffPhoneNumberHTML =
    "<tr>\n" +
    "  <td><select class='form-control' name='staff[phoneNumbers][{0}][type]'>\n" +
    "    <option value='mobile'>Mobile</option>\n" +
    "    <option value='home'>Home</option>\n" +
    "    <option value='work'>Work</option>\n" +
    "  </select></td>\n" +
    "  <td><input class='form-control' type='text' name='staff[phoneNumbers][{0}][number]'></td>\n" +
    "</tr>";

function appendPhoneNumber(tableId, formHTML)
{
    $("#"+tableId).append(formHTML.format(phoneNumbersCount));
    phoneNumbersCount++;
}

function appendEmergencyContact(tableId, formHTML)
{
    $("#"+tableId).append(formHTML.format(emergencyContactsCount));
    emergencyContactsCount++;
}

const Colors = ['#ff0000', '#ff4000', '#ff8000', '#ffbf00', '#ffff00', '#ffff00', '#ffff00',
    '#40ff00', '#40ff00', '#00ff40', '#00ff80', '#00ff80', '#00ff80', '#00ff80',
    '#0080ff', '#0040ff', '#0000ff', '#4000ff', '#8000ff', '#bf00ff', '#ff00ff', 
    '#ff00bf', '#ff0080', '#ff0040', '#ffffff', '#000000'];

function createSchedule() {
    let data = $('#booking-container').data('source');
    let rooms = $('#room-container').data('source');
    let patients = $('#patient-container').data('source');
    let filterMode = $('#filter-mode').val();
    let filterVal = $("#filter").val();

    if (filterVal) {
        if (filterMode == "room") {
            var currentRoom = '';
            rooms.forEach(room => {
                if(room.refNb == filterVal){
                    currentRoom = room._id.$oid;
                }
            });
            data = data.filter(booking => booking.room_id.$oid == currentRoom);
        } else if (filterMode == "patient") {
            var currentPatient = '';
            patients.forEach(patient => {
                if (patient.refNb == filterVal) {
                    currentPatient = patient._id.$oid;
                }
            });
            data = data.filter(booking => booking.patient_id.$oid == currentPatient);
        }
    }    

    // Event Mapper
    let bookings = data.map(booking => {
        // Formatted Dates
        let sDate = booking.startDt.split("T")[0];
        let sTime = booking.startDt.split("T")[1].split(".")[0];
        let eDate = booking.endDt.split("T")[0];
        let eTime = booking.endDt.split("T")[1].split(".")[0];
        // Notes
        let currentPatient = patients.filter(patient => patient._id.$oid == booking.patient_id.$oid);
        let patientName = currentPatient[0].personalDetails.name.first + " " + currentPatient[0].personalDetails.name.last;
        let currentRoom = rooms.filter(room => room._id.$oid == booking.room_id.$oid);
        let roomNo = currentRoom[0].refNb;

        event = {
            uid: booking._id.$oid,
            begins: sDate + " " + sTime,
            ends: eDate + " " + eTime,
            color: Colors[Math.floor(Math.random() * Colors.length)],
            notes: patientName + " - " + currentPatient[0].refNb + "\n" + roomNo + "\n" + booking.notes
        };

        return event;
    });

    // let divBackup = $('#schedule');
    // $('#schedule').remove();
    // $("#scheduleContainer").append(divBackup);

    $calendar = $('#schedule').cal({
        allowselect : true,
        eventselect : function( uid ) {
			console.log( 'Selected event: '+uid );
            var currentURL = window.location.href;
            console.log(" URL is " + currentURL);
            var urlComponents = currentURL.split("/");
            var newURL = urlComponents[0] + "/bookings/" + uid;
            console.log("New url is " + newURL);
            window.location.href = newURL;
		},
        events: bookings
    });

    $('#date-head').dateRange( $calendar.cal( 'option', 'startdate' ), $calendar.cal( 'option', 'startdate' ).addDays( $calendar.cal('option','daystodisplay')-1 ) );
}

$('#controls').on('click','button[name]',function() {
    let $this = $(this),
        action = $this.attr('name');

    // If this is already the current state, just exit.
    if( $this.is('.on') ) return;

    // Switch to the new state.
    switch( action ) {
        /**
         * TODO: For now... ideally you'd be able to toggle between views without reloading.
         */
        case 'day': window.location = 'day.html'; break;
        case 'year': window.location = 'year.html'; break;
        case 'month': window.location = 'month.html'; break;
        case 'prev': case 'today': case 'next':
        let today = $.cal.date();
        let starting = $calendar.cal('option','startdate');
        let duration = $calendar.cal('option','daystodisplay');
        let newstart = starting;
        switch( action ){
            case 'next' : newstart = starting.addDays(duration); $('button[name="today"]').parent().removeClass('on'); break;
            case 'prev'	: newstart = starting.addDays(0-duration); $('button[name="today"]').parent().removeClass('on'); break;
            case 'today': newstart = $.cal.date().addDays(1-$.cal.date().format('N')); break;
        }
        // Work out the new end date.
        let newend = newstart.addDays(duration-1);
        // Set the new startdate.
        $calendar.cal( 'option','startdate', newstart );

        if( today >= newstart && today <= newend )
            $('button[name="today"]').parent().addClass('on');

        // Set the new date in the header.
        $('#date_head').dateRange( newstart, newend )
        break;
    }
});

(function($){
	
	// The plugin name. Override if you find namespace collisions.
	var plugin_name = 'dateRange';
	
	// Set the plugin defaults.
	var defaults = {
		month		: 'jS',
		year		: 'jS M',
		full		: 'jS M Y',
		separator	: ' - '
	}
	
	/**
	 * The plugin function which does the date formatting magic.
	 *
	 * @param mixed start			: The start of the range. A date object, or a parseable date string.
	 * @param mixed end				: The end of the range. A date object, or a parseable date string.
	 * @param object options		: An object containing settings (date formats to print under different conditions).
	 * 
	 * @return jQuery Collection;
	 */
	$.fn[plugin_name] = function( start, end, options ){
		
		// Settings to the defaults.
		var settings = $.extend({},defaults);
		
		// Make sure these are extended date objects.
		start	= $.cal.date(start);
		end		= $.cal.date(end);
		
		// If options exist, lets merge them
		// with our default settings.
		if( options ) $.extend( settings, options )
		
		var diffDays	= start.format('Ymd') != end.format('Ymd'),
			diffMonths	= diffDays ? start.format('Ym') != end.format('Ym') : false,
			diffYears	= diffMonths ? start.format('Y') != end.format('Y') : false,
			startFormat	= diffYears || !diffDays ? settings.full : ( diffMonths ? settings.year : settings.month );
		
		// Return the formatted date.
		return this.text(start.format(startFormat)+( diffDays ? settings.separator+end.format(settings.full) : '' ));
	}
	
})(jQuery);


