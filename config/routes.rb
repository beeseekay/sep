Rails.application.routes.draw do
  get 'sessions/new'
  resources :bookings
  resources :rooms
  resources :patients
  resources :staffs, path: 'staff'
  resources :triage_queues, path: 'triage-queue', only: [:index, :create, :destroy]
  put "triage-queue/reorder", to: 'triage_queues#reorder'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  root 'patients#index'
end
